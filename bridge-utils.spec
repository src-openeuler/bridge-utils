Summary:        Utilities for configuring the linux ethernet bridge
Name:           bridge-utils
Version:        1.7.1
Release:        3
License:        GPL-2.0-or-later
URL:            https://wiki.linuxfoundation.org/networking/bridge
Source0:        https://git.kernel.org/pub/scm/network/bridge/bridge-utils.git/snapshot/%{name}-%{version}.tar.gz
Patch0000:      bugfix-bridge-not-check-parameters.patch
Patch0001:      bugfix-avoid-showmacs-memory-leak.patch

BuildRequires:  libsysfs-devel autoconf automake libtool
BuildRequires:  kernel-headers >= 2.6.16

%description
This package provides utilities for configuring the linux ethernet
bridge. The linux ethernet bridge can be used for connecting multiple
ethernet devices together.


%prep
%autosetup -n %{name}-%{version} -p1

%build
autoconf
%configure
%make_build

%install
%make_install SUBDIRS="brctl doc"

%files
%license COPYING
%doc AUTHORS doc/FAQ doc/HOWTO
%{_sbindir}/brctl
%{_mandir}/man8/brctl.8*

%changelog
* Thu Aug 01 2024 xinghe <xinghe2@h-partners.com> - 1.7.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:convert license to SPDX

* Wed Oct 19 2022 xinghe <xinghe2@h-partners.com> - 1.7.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove useless file

* Mon Mar 21 2022 quanhongfei <quanhongfei@h-partners.com> - 1.7.1-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC: update bridge-utils version to 1.7.1

* Tue Sep 8 2020 lunankun <lunankun@huawei.com> - 1.7-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix source0 url

* Wed Jul 22 2020 gaihuiying <gaihuiying1@huawei.com> - 1.7-1
- Type:enhancement
- ID:NA
- SUG:restart
- DESC: update bridge-utils version to 1.7

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.6.4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: delete patches

* Wed Sep 18 2019 Alex Chao <zhaolei746@huawei.com> - 1.6-3
- Package init
